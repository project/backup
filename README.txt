
Backup Module
=============

I manage some Drupal-based websites for non-profits, and wanted to give
the non-technical people who run the organizations a way to back up their
data over the web, in case I get hit by a beer truck or something.

This module is pretty simple, it allows you to download a backup of your
Drupal DOCUMENT_ROOT and database over the web.


Author
======

Douglas T. Muth - http://www.claws-and-paws.com/

Changelog
=========

A list of major changes made to the module:

16 Mar 2007: Converted to Drupal 5.

1 Oct 2006: Converted from 4.6 to 4.7.  Credit goes to the drupal.org user
	"elraun", who wrote the original patch.


